<h1 align="center">Buildit Interview Exercise</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: UNLICENSED" src="https://img.shields.io/badge/License-UNLICENSED-yellow.svg" />
  </a>
</p>

> A small nodejs application that crawls a website and for each page on the website gathers the links to other pages and assets, and displays lists of links to images, assets, other pages in the site and other pages external to the site.

### Contents
* README.md - this file
* package.json, package-lock.json - npm project and dependency definitions
* index.js - Nodejs command-line script to execute the application
* functions.js - Small library of utility functions to retrieve and parse webpages
* functions.test.js - Unit tests for the utility functions run with Jest framework
* test.html, page.html - HTML pages used in the unit tests

## Install

```sh
npm install
```

## Usage

```sh
node index.js --url <website>

eg

node index.js --url https://wiprodigital.com
node index.js --url https://wiprodigital.com > out.txt
```

## Run tests

```sh
npm run test
```

## Author

👤 **Mike Evans**

## TODO
* Better unit test coverage of failure cases, automated end-to-end tests
* Upgrade to crawl single-page applications running in the browser - could use puppeteer for this
* Deal with robots.txt on websites
* Improved logging/output other than console.log, better output formatting
* Concurrent page crawling
* Testing on many more websites - only been tested during development on https://wiprodigital.com and https://www.le.ac.uk/oerresources/bdra/html/index.htm
* Improve comments and code quality

