#!/usr/bin/env node
const url = require('url');
const functions = require('./functions');
const log = console.log;
const argv = require('optimist')
    .usage('Usage: $0 --url [url]')
    .demand(['url'])
    .describe('url', 'URL of a server generated website to be crawled')
    .argv;

const host = new URL(argv.url);
let visited = [];

// Crawl the URL
crawl(host.href);

// Recursive function to crawl a web page
async function crawl(url) {

    const this_page = new URL(url);

    // Get the trimmed pathname to check against the visited array
    pathname = this_page.pathname.replace(/^\/+|\/+$/g, '');

    // Don't visit the same page twice
    if (visited.includes(pathname)) {
        return;
    }

    // Add the trimmed pathname to the visited array
    visited.push(pathname);

    // Log the page header
    log('-----------------------------------------------------------')
    log('\t' + url);
    log('-----------------------------------------------------------')

    // Get the html of the page to crawl
    const html = await functions.getPage(url);

    if (!html) return;

    // Get the asset links
    const imgs = functions.parseImgSrcs(html);
    printArray(imgs, 'IMAGES')

    // Get the asset links
    const links = functions.parseLinks(html);
    printArray(links, 'LINKS')

    // Get all of the <a href> anchors
    const hrefs = functions.parseAnchors(html);

    if (hrefs.length) {
        // Filter and print external pages
        const external = hrefs.filter(h => h.startsWith('http') && ! h.startsWith(host.origin));
        printArray(external, 'EXTERNAL PAGES')

        // Filter and print internal pages on this site
        const internal = hrefs.filter(h => !h.startsWith('http') || h.startsWith(host.origin));
        printArray(internal, 'INTERNAL PAGES')

        // Crawl internal pages
        for (href of internal) {
            // Get the absolute URL for this internal page
            const page = new URL(href, this_page.href);
            await crawl(page.href);
        }
    }
}

// Function to print our an array of crawled items
function printArray(array, name) {
    if (array.length) {
        log(name + ':');
        array.forEach(h => {
            // Don't print all the anchor links to the same page
            if (!h.startsWith('#')) log(h)
        });
        log('');
    }
}

