const axios = require('axios');
const fs = require('fs');
const log = console.log;

const obj = {

    parseAnchors: function(html) {
        const regex = /<a.*href\s*=\s*(['"])(.+?)\1/ig;
        let foundLinks = []
        while (found = regex.exec(html)) {
            foundLinks.push(found[2]);
        }
        return foundLinks;
    },

    parseLinks: function(html) {
        const regex = /<link.*href\s*=\s*(['"])(.+?)\1/ig;
        let foundLinks = []
        while (found = regex.exec(html)) {
            foundLinks.push(found[2]);
        }
        return foundLinks;
    },

    parseImgSrcs: function(html) {
        const regex = /<img.*src\s*=\s*(['"])(.+?)\1/ig;
        let foundLinks = []
        while (found = regex.exec(html)) {
            foundLinks.push(found[2]);
        }
        return foundLinks;
    },

    getPage: async function(url) {
        try {
            const obj = await axios.get(url);
            if (obj && obj.status === 200) return obj.data;
        } catch(error) {
            if (error.response) {
                console.log('getPage ERROR  status: ', error.response.status + ' url: ' + url);
            }
        }
    }
};

module.exports = obj;


