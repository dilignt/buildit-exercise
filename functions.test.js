const parseAnchors = require('./functions').parseAnchors;
const parseLinks = require('./functions').parseLinks;
const parseImgSrcs = require('./functions').parseImgSrcs;
const getPage = require('./functions').getPage;
const fs = require('fs');

test('parseAnchors', () => {
    const links = [ 'page_01.htm', 'page_02.htm', 'page_03.htm', 'page_04.htm', 'page_05.htm', 'http://www.wimba.com/products/coursegenie' ];
    const html = fs.readFileSync('./test.html', 'utf8');
    expect(parseAnchors(html)).toStrictEqual(links);
});

test('parseLinks', () => {
    const links = [ 'cg.css' ];
    const html = fs.readFileSync('./test.html', 'utf8');
    expect(parseLinks(html)).toStrictEqual(links);
});

test('parseImgSrcs', () => {
    const links = [ '/imgs/test.png' ]
    const html = fs.readFileSync('./test.html', 'utf8');
    expect(parseImgSrcs(html)).toStrictEqual(links);
});

test('getPage', () => {
    const url = 'https://www.le.ac.uk/oerresources/bdra/html/index.htm';
    const html = fs.readFileSync('./page.html', 'utf8');
    const ret = expect(getPage(url)).resolves.toBe(html);
});
